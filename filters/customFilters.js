angular.module('sportsStore').filter('unique', function () {
    return function (data, property) {
        if(angular.isArray(data) && angular.isString(property)) {
            var keys = {},
                results = [],
                val,
                i = 0, l = data.length;
            for(; i < l; ++i) {
                val = data[i][property];
                if(angular.isUndefined(keys[val])) {
                    keys[val] = true;
                    results.push(val);
                }
            }
            return results;
        }
        return data;
    };
});