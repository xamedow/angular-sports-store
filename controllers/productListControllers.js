angular.module('sportsStore')
    .controller('productListCtrl', function ($scope) {
        var selectedCategory = null;

        $scope.selectCategory = function (category) {
            selectedCategory = category;
        };

        $scope.categoryFilterFn = function (product) {
            return selectedCategory == null || product.category == selectedCategory;
        };

        $scope.setActiveButton = function (category) {
            return category == selectedCategory ? 'btn-primary' : null;
        };
    });