angular.module('cart', []).factory('cart', function () {
    var cartData = [];
    return {
        addProduct: function (id, name, price) {
            var addedToExistingItem = false,
                i = 0, l = cartData.length;
            for(; i < l; ++i) {
                if(cartData[i].id === id) {
                    ++cartData[i].count;
                    addedToExistingItem = true;
                    break;
                }
            }
            if(!addedToExistingItem) {
                cartData.push({
                    count: 1,
                    id: id,
                    price: price,
                    name: name
                });
            }
        },

        removeProduct: function (id) {
            var i = 0, l = cartData.length;
            for(; i < l; ++i) {
                if(cartData[i].id === id) {
                    cartData.splice(i, 1);
                    break;
                }
            }
        },

        getProducts: function () {
            return cartData;
        }
    };
}).directive('cartSummary', function(cart) {
    return {
        restrict: 'E',
        templateUrl: 'components/cart/cartSummary.html',
        controller: function ($scope) {
            var cartData = cart.getProducts(),
                i = 0, l = cartData.length;
            $scope.total = function () {
                var total = 0;
                for(; i < l; ++i) {
                    total += cartData[i].price * cartData[i].count;
                }
                return total;
            };

            $scope.itemCount = function () {
                var total = 0;
                for(i = 0; i < l; ++i) {
                    total += cartData[i].count;
                }
                return total;
            }
        }
    };
});